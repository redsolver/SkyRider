var window = self;

var unzip = require('unzip-js')


const difficulties = [];

const xhrs = {};

// Fetch and unzip.
addEventListener('message', function (evt) {
  const difficulties = JSON.parse(evt.data.difficulties);
  const version = evt.data.version;
  const hash = evt.data.hash;

  const [short] = version.split('-');

  unzip(evt.data.directDownload, function (err, zipFile) {
    if (err) {
      return console.error(err)
    }

    zipFile.readEntries(function (err, entries) {
      if (err) {
        return console.error(err)
      }

      const data = {
        audio: undefined,
        beats: {}
      };

      const beatFiles = {};

      /* TODO      difficulties.forEach((diff, i) => {
              data.beats[diff] = values[i];
            }); */

      // console.log(entries);

      entries.forEach(function (entry) {
        // console.log(entry);
        const chunks = [];


        zipFile.readEntryData(entry, false, function (err, readStream) {
          if (err) {
            return console.error(err)
          }

          readStream.on('data', function (chunk) { chunks.push(chunk) })
          // readStream.on('error', function (err) { ... })
          readStream.on('end', function () {
            console.log(entry.name);

            if (entry.name.endsWith('.egg') || entry.name.endsWith('.ogg')) {
              var blob = new Blob(chunks, /* { type: 'application/octet-binary' } */); // ein gültiger MIME-Typ
              var url = URL.createObjectURL(blob);
              // console.log(url);

              data.audio = url;


            } else {

              var filename = entry.name;
              if (!filename.toLowerCase().endsWith('.dat')) return;


              var string =/*  resolve( */Buffer.concat(chunks).toString('utf8')// )
              var value = JSON.parse(string);


              if (filename.toLowerCase() === 'info.dat') {
                data.info = value;
              } else {
                // filename = filename.substring(0, filename.length - 4);

                // console.log(filename)

                value._beatsPerMinute = evt.data.bpm;
                beatFiles[filename] = value;

              }



              /*  if (!(filename.startsWith("Easy") || filename.startsWith("Expert") || filename.startsWith("Hard") || filename.startsWith("Normal") || filename.startsWith("Info") || filename.startsWith("info"))) {
                 return;
               } */

              /* if (filename.endsWith("Standard.dat")) {
                filename = filename.substring(0, filename.length - 8);
              } */


            }

            if (data.audio === undefined) {
              return;
            }
            if (data.info === undefined) {
              return;
            }
            for (const difficultyBeatmapSet of data.info._difficultyBeatmapSets) {
              const beatmapCharacteristicName = difficultyBeatmapSet._beatmapCharacteristicName;

              for (const difficultyBeatmap of difficultyBeatmapSet._difficultyBeatmaps) {
                const difficulty = difficultyBeatmap._difficulty;
                const beatmapFilename = difficultyBeatmap._beatmapFilename;
                if (beatFiles[beatmapFilename] === undefined) {
                  return;

                }

                const id = beatmapCharacteristicName + '-' + difficulty;
                if (data.beats[id] === undefined) {
                  data.beats[id] = beatFiles[beatmapFilename];
                }

              }

            }


            /* console.log(data);
            console.log('done') */

            postMessage({ message: 'load', data: data, version: version, hash: hash });

          })
        })
      })
    })
  })
  return;



});

// data: {audio url, beats { difficulty JSONs },
